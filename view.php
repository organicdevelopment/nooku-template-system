<?php
/**
 * Created By: Oli Griffiths
 * Date: 19/12/2012
 * Time: 16:12
 */
defined('KOOWA') or die('Protected resource');

class TmplKoowaView extends KViewHtml
{
	/**
	 * Constructor
	 *
	 * @param KConfig|null $config  An optional KConfig object with configuration options
	 * @return \KObjectDecorator
	 */
	public function __construct(KConfig $config)
	{
		parent::__construct($config);

		//Set the template data
		$this->_data            = $config->data->toArray();
		$this->_data['params']  = $this->getParams();
		$this->_data['document']= $config->document;

		//Add alias filter for media:// namespace
		$this->getTemplate()->getFilter('alias')->append(
			array(
				//'template://' => $config->tmpl_url.'/',
				'assets://' => $config->tmpl_url.'/assets/',
				'base://' => $config->base_url.'/',
				'@route(' => 'JRoute::_('
			),
			KTemplateFilter::MODE_READ | KTemplateFilter::MODE_WRITE
		);

		//Setup the layout
		$this->_data['layout'] = $this->getComponentLayout();
	}



	/**
	 * Initializes the config for the object
	 *
	 * Called from {@link __construct()} as a first step of object instantiation.
	 *
	 * @param   object  An optional KConfig object with configuration options
	 * @return  void
	 */
	protected function _initialize(KConfig $config)
	{
		$identifier = $this->getIdentifier();
		$config->append(array(
			'auto_assign'       => false,
			'document'          => JFactory::getDocument(),
			'base_url'          => KRequest::base(),
			'tmpl_url'          => KRequest::base().'/templates/'.$identifier->package,
			'media_url'         => KRequest::root().'/media',
			'template_filters'  => array('head','meta','modules','component'),
			'tmpl'              => KRequest::get('request.tmpl','string','default'),
			'data'              => array(
				'option'    => KRequest::get('request.option','string'),
				'messages'  => $this->getMessages(),
				'language'  => JFactory::getConfig()->get('language'),
				'direction' => 'ltr'
			)
		))->append(array(
			'data' => array(
				'tmpl'      => $config->tmpl
			)
		))->append(array(
			'data' => array(
				'cache'     => $config->data->tmpl != 'login'
			)
		));

		parent::_initialize($config);
	}

	/**
	 * Get the name
	 *
	 * @return 	string 	The name of the object
	 */
	public function getName()
	{
		return $this->getIdentifier()->name;
	}

	/**
	 * Get the identifier for the template with the same name
	 *
	 * @return  KTemplate
	 */
	public function getTemplate()
	{
		if(!$this->_template instanceof KTemplateAbstract)
		{
			//Make sure we have a template identifier
			if(!($this->_template instanceof KServiceIdentifier)) {
				$this->setTemplate($this->_template);
			}

			$options = array(
				'view' => $this,
				'cache' => $this->cache
			);

			$this->_template = $this->getService($this->_template, $options);
		}

		return $this->_template;
	}


	/**
	 * Returns the template params
	 * @return KConfig
	 */
	public function getParams()
	{
		return new KConfig(isset($this->document->params) && ($this->document->params instanceof JRegistry
			? $this->document->params->toArray()
			: array()
		));
	}


	public function getMessages()
	{
		$messages = JFactory::getApplication()->getMessageQueue();
		$return = array();

		foreach($messages AS $message){
			if(!isset($return[$message['type']])) $return[$message['type']] = array();
			$return[$message['type']][] = $message['message'];
		}

		return $return;
	}


	public function countMessages()
	{
		return count( JFactory::getApplication()->getMessageQueue() );
	}


	public function getComponentLayout()
	{
		static $layout;

		if($layout) return $layout;

		$package = preg_replace('#^com_#','',KRequest::get('request.option','string'));
		$layout = KRequest::get('request.layout','string');
		$view = KRequest::get('request.view','string');
		$task = KRequest::get('request.task','string');
		if($layout == 'edit' || $task == 'edit' || preg_match('#^edit#', $task)) $layout = 'joomla-form';

		if($layout) return $layout;

		try{
			$identifier = clone $this->getIdentifier();
			$identifier->type = 'com';
			$identifier->package = $package;
			$identifier->path = array();
			$identifier->name = 'dispatcher';

			$controller = $this->getService($identifier)->getController();

			//Components that don't have a default controller, extend KControllerService and thus must have a DB table
			if($controller instanceof KControllerService){
				if(!$controller->getModel()->isConnected()){
					throw new KException('');
				}
			}

			$layout = $controller->getView()->getLayout();
		}catch(KException $e)
		{
			if(!$view){
				$class = ucfirst($package).'Controller';
				if(class_exists($class)){
					$controller = new $class;
					$props = method_exists($controller, 'getProperties') ? $controller->getProperties() : array();
					$view = isset($props['default_view']) ? $props['default_view'] : null;
				}
			}

			$layout = !$view || $view && KInflector::isSingular($view) ? 'joomla-form' : 'default';
		}

		return $layout;
	}
}
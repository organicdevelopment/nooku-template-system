<?php
/**
 * Modules Template Filter Class
 *
 * @author    	Oli Griffiths <oli@organic-development.com>
 * @package     Nooku
 * @subpackage  Nooku Template
 */
class TmplKoowaTemplateFilterModules extends KTemplateFilterAbstract implements KTemplateFilterWrite, KTemplateFilterRead
{
	protected $_document;
	protected $_renderer;
	protected $_modules = array();
	protected $_module_counts = array(); //Holds counts for inline module positions prior to write()


	/**
	 * Object constructor
	 * @param KConfig $config
	 */
	public function __construct(KConfig $config = null)
	{
		parent::__construct($config);

		$this->_document = JFactory::getDocument();
		$this->_renderer = $this->_document->loadRenderer('module');
	}


	/**
	 * Initializes the options for the object
	 *
	 * Called from {@link __construct()} as a first step of object instantiation.
	 *
	 * @param   object  An optional KConfig object with configuration options
	 * @return void
	 */
	protected function _initialize(KConfig $config)
	{
		$config->append(array(
			'priority'   => KCommand::PRIORITY_HIGH,
		));

		parent::_initialize($config);
	}


	/**
	 * Parse <khtml:module /> tags and set positions for tags nested in <ktml:modules> positions
	 *
	 * @param string Block of text to parse
	 * @return KTemplateFilterRead
	 */
	public function read(&$text)
	{
		$this->_setNestedModulesPosition($text);

		//Merge in all document buffers
		if(preg_match_all('#<ktml:modules\s+position="([^"]+)"(.*)#iU', $text, $matches)){
			foreach($matches[1] AS $position){

				if(isset(JDocument::$_buffer['modules']) && isset(JDocument::$_buffer['modules'][$position])){

					//Create module object
					$module = new KConfig(array(
						'id'        => uniqid(),
						'identifier'=> $this->getIdentifier('mod://site/default.module.default.html'),
						'position'  => $position,
						'content'   => JDocument::$_buffer['modules'][$position],
						'attribs'   => array(),
						'params'    => array()
					));

					$this->_modules[$position][] = $module;
				}
			}
		}

		return $this;
	}


	/**
	 * Parse <khtml:module /> tags
	 * Parse <khtml:modules /> and <khtml:modules></khtml:modules> tags
	 *
	 * @param string Block of text to parse
	 * @return ComPagesTemplateFilterModule
	 */
	public function write(&$text)
	{
		$this->_parseModuleTags($text);
		$this->_parseModulesTags($text);
		return $this;
	}


	/**
	 * Parses single module tags
	 * @param $text
	 * @return mixed
	 */
	protected function _parseModuleTags(&$text)
	{
		$text = preg_replace_callback('#<ktml:module\s+([^>]*)\s*/>#siU', array($this, '_replaceModule'), $text);
		$text = preg_replace_callback('#<ktml:module\s+([^>]*)>(.*)</ktml:module>#siU', array($this, '_replaceModule'), $text);
		$this->_module_counts = array();

		return $text;
	}


	/**
	 * Parses out modules
	 * @param $text
	 * @return TmplKoowaTemplateFilterModules
	 */
	protected function _parseModulesTags(&$text)
    {
	    $text = preg_replace_callback('#<ktml:modules\s+position="([^"]+)"(.*)\/>#iU', array($this, '_replaceModules'), $text);
	    $text = preg_replace_callback('#<ktml:modules\s+position="([^"]+)"(.*)>(.*)</ktml:modules>#siU', array($this, '_replaceModules'), $text);

        return $this;
    }


	/**
	 * Sets the position property on nested modules
	 * @param $text
	 */
	protected function _setNestedModulesPosition(&$text)
	{
		//Sets the position on all nested modules within module positions if they're missing position
		if(preg_match_all('#<ktml:modules\s*(.*)>(.*)</ktml:modules>#siU', $text, $positions)){

			//Loop all module positions
			foreach($positions[0] AS $key => $position){

				if(preg_match_all('#<ktml:module\s+([^>]*)\/+>#siU', $positions[2][$key], $modules)){

					$position_attributes = $this->_parseAttributes($positions[1][$key]);

					//Check for any modules contained in this position, and set the position name if not set
					if(isset($position_attributes['position'])){

						foreach($modules[0] AS $key2 => $module){
							$attributes = $this->_parseAttributes($modules[1][$key2]);

							//If position is set, continue on
							if(isset($attributes['position'])) continue;

							//Set position
							$attributes['position'] = $position_attributes['position'];

							//If the module appears before the <ktml:content tag, prepend the module
							$contentPos = strpos($position, '<ktml:content');
							if($contentPos !== null && !isset($attributes['content'])){
								$modulePos = strpos($position, $module);
								if($modulePos < $contentPos){
									$attributes['content'] = 'prepend';
								}
							}

							//Convert back to html attributes
							$attributes = KHelperArray::toString($attributes);

							//Replace the origin module
							$module = str_replace($modules[1][$key2], $attributes, $module);

							//Replace in positions array
							$position = str_replace($modules[0][$key2], $module, $position);

							//Incrememnt position count
							if(!isset($this->_module_counts[$position_attributes['position']])) $this->_module_counts[$position_attributes['position']] = 0;
							$this->_module_counts[$position_attributes['position']]++;
						}

						//Replace module position
						$text = str_replace($positions[0][$key], $position, $text);
					}
				}
			}
		}
	}


	/**
	 * Module replacement function
	 * @param $match
	 * @return mixed|string
	 */
	public function _replaceModule($match)
	{
		//Check for attributes
		if($attributes = trim($match[1])){
			$attributes = new KConfig($this->_parseAttributes($attributes));
		}else{
			$attributes = new KConfig();
		}

		$attributes->append(array(
			'title'		=> '',
			'showtitle' => '',
			'class'		=> '',
			'position'  => '',
			'module'    => 'mod_default'
		));

		$content = null;
		if(isset($match[2])){
			//$tmpl = $this->getTemplate();
			// $content = (string) $this->getTemplate()->loadString($match[2], $attributes->toArray());
            $content = $match[2];
			//$this->_template = $tmpl;

		}else if($attributes->module){

			//If the moduel doesnt start mod_ try to load it as an identifier from the modules folder
			if(substr($attributes->module, 0, 4) != 'mod_'){
				try{
					$identifier = clone $this->getIdentifier();
					$identifier->path = 'module';
					$identifier->name = $attributes->module;

					//Must get a new template instance else $this->_template gets erased
					$tmpl = $this->getTemplate();
					$content = (string) $tmpl->loadIdentifier($identifier, $attributes->toArray());
					$this->_template = $tmpl;

				}catch(KException $e){
					$content = $e->getMessage();
				}
			}
		}else{
			$content = null;
		}

		//Create module object
		$module = (object) $attributes->append(array(
			'id'        => uniqid(),
			'identifier'=> $this->getIdentifier('mod://site/default.module.default.html')
		))->toArray();


		//Set the content
		$module->content = $content;
		$module->attribs = $attributes->toArray();
		$module->params = $attributes;

		if(!isset($this->_document->modules[$module->position])) $this->_document->modules[$module->position] = array();
		$this->_document->modules[$module->position][] = $module;
	}


	/**
	 * Modules replacement function
	 * @param $match
	 * @return mixed|string
	 */
	public function _replaceModules($match)
	{
		$position = $match[1];

		//Check for attributes
		if($attributes = trim($match[2])){
			$attributes = new KConfig($this->_parseAttributes($attributes));
		}else{
			$attributes = new KConfig();
		}

		//Render
		$content = $this->render($position, $attributes);

		//check for <ktml:content /> replacement
		if(isset($match[3]) && preg_match('#<ktml:content\s*\/>#', $match[3]) && trim($content)){
			$content = preg_replace('#<ktml:content\s*\/>#', $content, $match[3]);
		}

		return $content;
	}


	/**
	 * Loads the modules in a module position
	 * @param $position
	 * @return mixed
	 */
	public function load($position)
	{
		if(!isset($this->_modules[$position])){

			//Get the modules
			$this->_modules[$position] = JModuleHelper::getModules($position);

			//Merge modules in the document _modules property
			if(isset($this->_document->modules[$position]))
			{
				foreach($this->_document->modules[$position] as $module)
				{
					if(isset($module->attribs['content']) && $module->attribs['content'] == 'prepend'){
						array_unshift($this->_modules[$position], $module);
					}else{
						array_push($this->_modules[$position], $module);
					}
				}
			}

			//Process module params into KConfigs
			foreach($this->_modules[$position] AS &$module){
				$module->params = $this->processParams($module->params);
			}
		}

		return $this->_modules[$position];
	}


	/**
	 *
	 * @param $position
	 * @return int
	 */
	public function count($position)
	{
		return count($this->load($position)) + (isset($this->_module_counts[$position]) ? $this->_module_counts[$position] : 0);
	}


	/**
	 * Process parameters into KConfigs, can be a json or INI string
	 * @param $params
	 * @return KConfig
	 */
	protected function processParams($params)
	{
		if(is_string($params)){
			if($json = json_decode($params, true)){
				$params = new KConfig($json);
			}else{
				$handler = JRegistryFormat::getInstance('INI');
				$params = new KConfig((array) $handler->stringToObject($params));
			}

			//Convert moduleclass_sfx
			if($params->moduleclass_sfx){
				$params->class = $params->moduleclass_sfx;
				unset($params->moduleclass_sfx);
			}

			//Convert class parameters into module params
			$classes = explode(' ',$params->class);
			foreach($classes AS $i => $cls){
				$parts = explode('=', $cls, 2);
				if(count($parts) == 2){
					$params->$parts[0] = $parts[1];
					unset($classes[$i]);
				}
			}

			$params->class = implode(' ', $classes);
		}

		return $params;
	}


	/**
	 * Renders a module position
	 * @param $position
	 * @param array $config
	 * @return string
	 */
	public function render($position, $config = array())
	{
		$config = new KConfig($config);
		$config->append(array(
			'chrome' => 'default',
			'rel' => array(),
			'position' => $position
		));
		$modules = $this->load($position);


		$output = array();
		$count = 1;
		foreach($modules AS $module)
		{
			//Set the module attributes
			if($count == 1) {
				$config->rel->first = 'first';
			}

			if($count == count($modules)) {
				$config->rel->last = 'last';
			}

			//Append position config into module params
			$module->attribs = $config;
			$module->params->append($config);

			//Special handling for showtitle
			if($config->showtitle !== null) $module->showtitle = $config->showtitle;

			//Render module
			$content = $this->renderModule($module, $config);

			//Add to output
			$output[] = $content;

			$count++;
		}

		return implode("\n", $output);
	}


	/**
	 * Renders a module and applies chrome if set
	 * @param $module
	 * @param $params
	 * @return mixed
	 */
	public function renderModule($module, $params)
	{
		$module->content = $module->content ?: $this->_renderer->render( $module, $params->toArray() );

		$content = $module->content;

		//Chrome module
		if($params->chrome) $content = $this->chromeModule($module, $params);

		return $content;
	}


	/**
	 * Chromes a module. Chromes are stored in the template/layouts/module
	 * @param $module
	 * @param KConfig $params
	 * @return mixed
	 */
	public function chromeModule($module, $params)
	{
		//Construct chrome identifier
		$identifier = clone $this->getTemplate()->getIdentifier();
		$identifier->path = array('tmpl', 'module');
		$identifier->name = $module->params->get('chrome','default');

		//Render the module chrome
		$options = array(
			'module'        => $module,
			'position'      => $params->position,
			'title'         => $module->title,
			'params'        => $module->params,
			'class'         => $module->params->class,
			'first'         => $params->rel->first,
			'last'          => $params->rel->last,
			'config'        => $params,
			'content'       => $module->content ?: $params->content
		);

		$tmpl = $this->getTemplate();
		$content = (string) $tmpl->loadIdentifier($identifier, $options);
		$this->_template = $tmpl;
		return $content;
	}
}
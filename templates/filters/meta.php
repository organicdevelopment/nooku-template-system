<?php
/**
 * @version     $Id: link.php -1 1970-01-01 00:00:00Z  $
 * @package     Nooku_Server
 * @subpackage  Application
 * @copyright   Copyright (C) 2007 - 2012 Johan Janssens. All rights reserved.
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.nooku.org
 */

/**
 * Mate Template Filter Class
 *
 * @author    	Johan Janssens <http://nooku.assembla.com/profile/johanjanssens>
 * @package     Nooku_Server
 * @subpackage  Application
 */
class TmplKoowaTemplateFilterMeta extends KTemplateFilterAbstract implements KTemplateFilterWrite
{
	/**
	 * Initializes the options for the object
	 *
	 * Called from {@link __construct()} as a first step of object instantiation.
	 *
	 * @param   object  An optional KConfig object with configuration options
	 * @return void
	 */
	protected function _initialize(KConfig $config)
	{
		$config->append(array(
			'priority'   => KCommand::PRIORITY_LOW,
		));

		parent::_initialize($config);
	}


	/**
	 * Parse the text and filter it
	 *
	 * @param string Block of text to parse
	 * @return KTemplateFilterWrite
	 */
    public function write(&$text)
    {
        $meta = $this->_parseTags($text);
        $text = str_replace('<ktml:meta />'."\n", $meta, $text);

        return $this;
    }

	/**
	 * Parse the text for script tags
	 *
	 * @param string Block of text to parse
	 * @return string
	 */
	protected function _parseTags(&$text)
	{
		$tags = '';

		$matches = array();
		if(preg_match_all('#<meta\s*content="([^"]+)"(.*)\/>#iU', $text, $matches))
		{
			foreach(array_unique($matches[1]) as $key => $match)
			{
				//Set required attributes
				$attribs = array(
					'content' => $match
				);

				$attribs = array_merge($this->_parseAttributes( $matches[2][$key]), $attribs);
				$tags .= $this->_renderTag($attribs);
			}

			$text = str_replace($matches[0], '', $text);
		}

		return $tags;
	}

	/**
	 * Render the tag
	 *
	 * @param 	array	Associative array of attributes
	 * @param 	string	The tag content
	 * @return string
	 */
	protected function _renderTag($attribs = array(), $content = null)
	{
		$attribs = KHelperArray::toString($attribs);

		$html = '<meta '.$attribs.' />'."\n";
		return $html;
	}
}
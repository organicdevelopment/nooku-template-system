<?php
/**
 * Created By: Oli Griffiths
 * Date: 06/11/2012
 * Time: 18:30
 */
defined('KOOWA') or die('Protected resource');


/**
 * Component Template Filter Class
 *
 * @author    	Oli Griffiths
 * @package     Koowa
 * @subpackage  Application
 */
class TmplKoowaTemplateFilterComponent extends KTemplateFilterLink
{
    /**
     * Initializes the options for the object
     *
     * Called from {@link __construct()} as a first step of object instantiation.
     *
     * @param   object  An optional KConfig object with configuration options
     * @return void
     */
    protected function _initialize(KConfig $config)
    {
        $config->append(array(
            'priority'   => KCommand::PRIORITY_HIGHEST,
        ));

        parent::_initialize($config);
    }


	public function write(&$text)
	{
        $component = $this->getTemplate()->getView()->document->getBuffer('component');
        $component = str_replace('<script type="text/javascript">',   '<script data-inline type="text/javascript">', $component);
        $component = str_replace('<script type=\'text/javascript\'>', '<script data-inline type="text/javascript">', $component);

        if(preg_match_all('#<script\s*([^>]*)>#s', $component, $matches))
        {
            foreach($matches[0] as $key => $match)
            {
                $attributes = $this->_parseAttributes(str_replace("'", '"', $matches[1][$key]));

                if(!isset($attributes['data-inline']) && !isset($attributes['src']))
                {

                    $attribs =  'data-inline ' .KHelperArray::toString($attributes);
                    $script = str_replace($matches[1][$key], $attribs, $match);
                    $component = str_replace($match, $script, $component);

                }
            }
        }

        $text = str_replace('<ktml:component />', $component."\n", $text);

		return $this;
	}
}
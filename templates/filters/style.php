<?php
/**
 * @version     $Id: link.php -1 1970-01-01 00:00:00Z  $
 * @package     Nooku_Server
 * @subpackage  Application
 * @copyright   Copyright (C) 2007 - 2012 Johan Janssens. All rights reserved.
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.nooku.org
 */

/**
 * Style Template Filter Class
 *
 * @author    	Johan Janssens <http://nooku.assembla.com/profile/johanjanssens>
 * @package     Nooku_Server
 * @subpackage  Application
 */
class TmplKoowaTemplateFilterStyle extends KTemplateFilterStyle
{
	/**
	 * Initializes the options for the object
	 *
	 * Called from {@link __construct()} as a first step of object instantiation.
	 *
	 * @param   object  An optional KConfig object with configuration options
	 * @return void
	 */
	protected function _initialize(KConfig $config)
	{
		$config->append(array(
			'priority'   => KCommand::PRIORITY_LOW,
		));

		parent::_initialize($config);
	}


	/**
	 * Parse the text and filter it
	 *
	 * @param string Block of text to parse
	 * @return KTemplateFilterWrite
	 */
    public function write(&$text)
    {
        $styles = $this->_parseStyles($text);
        $text = str_replace('<ktml:style />'."\n", $styles, $text);

        return $this;
    }


    /**
     * Render style information
     *
     * @param 	string	The style information
     * @param 	boolean	True, if the style information is a URL
     * @param 	array	Associative array of attributes
     * @return string
     */
    protected function _renderStyle($style, $link, $attribs = array())
    {
        $attribs = KHelperArray::toString($attribs);

        if(!$link)
        {
            $html  = '<style type="text/css" '.$attribs.'>'."\n";
            $html .= trim($style);
            $html .= '</style>'."\n";
        }
        else $html = '<link type="text/css" rel="stylesheet" href="'.$style.'" '.$attribs.' />'."\n";

        return $html;
    }
}
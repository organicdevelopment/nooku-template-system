<?php
/**
 * Created By: Oli Griffiths
 * Date: 06/11/2012
 * Time: 18:30
 */
defined('KOOWA') or die('Protected resource');


/**
 * HEAD Template Filter Class
 *
 * @author    	Oli Griffiths
 * @package     Nooku_Server
 * @subpackage  Application
 */
class TmplKoowaTemplateFilterHead extends KTemplateFilterAbstract implements KTemplateFilterWrite
{
	/**
	 * Replaced <ktml:head /> with joomla document head
	 * @param $text
	 * @return KTemplateFilterLink|TmplKoowaTemplateFilterHead
	 */
	public function write(&$text)
	{
		$head = $this->getTemplate()->getView()->document->getBuffer('head');
		$head = str_replace('></script>','/>', $head);
		$head = str_replace('type="text/javascript"','', $head);

		$text = str_replace('<ktml:head />'."\n", $head, $text);

		return $this;
	}
}
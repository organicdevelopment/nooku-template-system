<?php
/**
 * Created By: Oli Griffiths
 * Date: 06/11/2012
 * Time: 17:57
 */
defined('KOOWA') or die('Protected resource');

class TmplKoowaTemplateFilterTemplate extends KTemplateFilterAbstract implements KTemplateFilterRead
{
	/**
	 * Initializes the options for the object
	 *
	 * Called from {@link __construct()} as a first step of object instantiation.
	 *
	 * @param   object  An optional KConfig object with configuration options
	 * @return void
	 */
	protected function _initialize(KConfig $config)
	{
		$config->append(array(
			'priority'   => KCommand::PRIORITY_HIGH,
		));

		parent::_initialize($config);
	}


	/**
	 * Replace template alias with a fully qualified identifier
	 *
	 * This function only replaces relative identifiers to a full path
	 * based on the path of the template.
	 *
	 * @param string
	 * @return KTemplateFilterAlias
	 */
	public function read(&$text)
	{
		if(preg_match_all('#@template\([\'"](.*)[\'"]#siU', $text, $matches))
		{
			foreach($matches[1] as $key => $match)
			{
				if(is_string($match) && strpos($match, '.') === false )
				{
					$identifier = clone $this->getTemplate()->getIdentifier();
					$identifier->path = array();
					$identifier->name = $matches[1][$key];

					$text = str_replace($matches[0][$key], '$this->loadIdentifier('."'".$identifier."'", $text);
					$text = str_replace($matches[0][$key], '$this->loadIdentifier('.'"'.$identifier.'"', $text);
				}
			}
		}

		return $this;
	}
}
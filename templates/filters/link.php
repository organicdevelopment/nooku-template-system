<?php
/**
 * @version     $Id: link.php -1 1970-01-01 00:00:00Z  $
 * @package     Nooku_Server
 * @subpackage  Application
 * @copyright   Copyright (C) 2007 - 2012 Johan Janssens. All rights reserved.
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.nooku.org
 */

/**
 * Link Template Filter Class
 *
 * @author    	Johan Janssens <http://nooku.assembla.com/profile/johanjanssens>
 * @package     Nooku_Server
 * @subpackage  Application
 */
class TmplKoowaTemplateFilterLink extends KTemplateFilterLink
{
	/**
	 * Initializes the options for the object
	 *
	 * Called from {@link __construct()} as a first step of object instantiation.
	 *
	 * @param   object  An optional KConfig object with configuration options
	 * @return void
	 */
	protected function _initialize(KConfig $config)
	{
		$config->append(array(
			'priority'   => KCommand::PRIORITY_LOW,
		));

		parent::_initialize($config);
	}


	/**
	 * Parse the text and filter it
	 *
	 * @param string Block of text to parse
	 * @return KTemplateFilterWrite
	 */
    public function write(&$text)
    {
        $links = $this->_parseLinks($text);
        $text = str_replace('<ktml:link />'."\n", $links, $text);

        return $this;
    }

	/**
	 * Render script information
	 *
	 * @param string	The script information
	 * @param array		Associative array of attributes
	 * @return string
	 */
	protected function _renderScript($link, $attribs = array())
	{
		return $this->_renderLink($link, $attribs);
	}
}
<?php
/**
 * @version     $Id: link.php -1 1970-01-01 00:00:00Z  $
 * @package     Nooku_Server
 * @subpackage  Application
 * @copyright   Copyright (C) 2007 - 2012 Johan Janssens. All rights reserved.
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.nooku.org
 */

/**
 * Script Template Filter Class
 *
 * @author    	Johan Janssens <http://nooku.assembla.com/profile/johanjanssens>
 * @package     Nooku_Server
 * @subpackage  Application
 */
class TmplKoowaTemplateFilterScript extends KTemplateFilterScript
{
	/**
	 * Initializes the options for the object
	 *
	 * Called from {@link __construct()} as a first step of object instantiation.
	 *
	 * @param   object  An optional KConfig object with configuration options
	 * @return void
	 */
	protected function _initialize(KConfig $config)
	{
		$config->append(array(
			'priority'   => KCommand::PRIORITY_LOW,
		));

		parent::_initialize($config);
	}


	/**
	 * Parse the text and filter it
	 *
	 * @param string Block of text to parse
	 * @return KTemplateFilterWrite
	 */
    public function write(&$text)
    {
        $scripts = $this->_parseScripts($text);
        $text = str_replace('<ktml:script />'."\n", $scripts, $text);

        return $this;
    }






    protected function _parseScripts(&$text)
    {
        $scripts = array();

        $matches = array();
        // <script src="" />
        if(preg_match_all('#<script(?!\s+data\-inline\s*)\s+src="([^"]+)"(.*)/>#siU', $text, $matches))
        {
            foreach(array_unique($matches[1]) as $key => $match)
            {
                $attribs = $this->_parseAttributes( $matches[2][$key]);
                $scripts[] = $this->_renderScript($match, true, $attribs);
            }

            $text = str_replace($matches[0], '', $text);
        }

        $matches = array();
        // <script></script>
        if(preg_match_all('#<script(?!\s+data\-inline\s*)(.*)>(.*)</script>#siU', $text, $matches))
        {
            foreach($matches[2] as $key => $match)
            {
                $attribs = $this->_parseAttributes( $matches[1][$key]);
                $scripts[] = $this->_renderScript($match, false, $attribs);
            }

            $text = str_replace($matches[0], '', $text);
        }

        // Ensure that mootools is loaded first, then jquery, then other scripts
        $mootools = array();
        $jquery = array();
        $regular = array();

        foreach($scripts as $script)
        {
            if(preg_match('/mootools/', $script)) $mootools[] = $script;

            else if(preg_match('/mootools/', $script)) $jquery[] = $script;

            else $regular[] = $script;
        }

        $scripts = array_merge($mootools, $jquery, $regular);

        return implode('', $scripts);
    }
}
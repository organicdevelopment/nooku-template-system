<?php
/**
 * User: Oli Griffiths
 * Date: 24/06/2012
 * Time: 14:34
 */

class TmplKoowaTemplateDefault extends ComDefaultTemplateAbstract
{
	/**
	 * Constructor
	 *
	 * Prevent creating instances of this class by making the contructor private
	 *
	 * @param 	object 	An optional KConfig object with configuration options
	 */
	public function __construct(KConfig $config)
	{
		parent::__construct($config);

		if(!$config->cache){
			$this->_cache = null;
		}
	}

	/**
	 * Initialize the object
	 * @param KConfig $config
	 */
	protected function _initialize(KConfig $config)
	{
		$config->append(array(
			'cache' => true
		));
		parent::_initialize($config);
	}


	/**
	 * Loads an identifier. Attempts to get it from the current template, falls back to koowa template
	 * @param $template
	 * @param array $data
	 * @param bool $process
	 * @return KTemplateAbstract
	 */
	public function loadIdentifier($template, $data = array(), $process = true)
	{
		$template = $this->getIdentifier($template);
		if(empty($template->path)) $template->path = array('tmpl');

		try{
			return parent::loadIdentifier($template, $data, $process);
		}catch(KException $e){
			$template = clone $template;
			$template->application = 'site';
			$template->package = 'koowa';
			return parent::loadIdentifier($template, $data, $process);
		}
	}
}
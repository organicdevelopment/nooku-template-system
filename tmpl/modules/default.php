<?php
/**
 * User: Mark Head <mark@organic-development.com>
 * Date: 21/09/2012
 * Time: 12:56
 */

?>
<div class="module <?= $module->module; ?> <?= $first ? 'first' : '' ?> <?= $last ? 'last' : '' ?> <?= $class ?>">

    <?php if($params->badge) echo $params->badge_html ?>

    <?php if($module->showtitle): ?>
        <h3 class="title"><?= $title ?><?php if($params->icon) echo $params->icon_html ?></h3>
    <?php endif ?>

    <?= $content ?>
</div>
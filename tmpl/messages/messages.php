<?php
/**
 * User: Mark Head <mark@organic-development.com>
 * Date: 03/10/2012
 * Time: 16:04
 */

defined( 'KOOWA' ) or die( 'Restricted Access' );

// Twitter bootstrap helper
@helper( 'bootstrap.style', array( 'alert' ) );
@helper( 'bootstrap.script', array( 'alert' ) );

foreach( $messages as $message )
{
    $identifier = clone $this->getIdentifier();
    $identifier->path = array('layout', 'message', 'chrome');
    $identifier->name = 'default';

    $message = new KConfig($message);

    // Render the messages template
    echo @template($identifier, array(
        'message'   => $message
    ));
}

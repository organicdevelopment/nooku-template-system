<?php
/**
 * User: Mark Head <mark@organic-development.com>
 * Date: 03/10/2012
 * Time: 16:08
 */

defined( 'KOOWA' ) or die( 'Restricted Access' );
?>

<div class="message message-<?= $message->type; ?>">
    <?= $message->message; ?>
</div>
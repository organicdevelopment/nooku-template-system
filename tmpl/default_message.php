<?
/**
 * @version     $Id: default.php 4558 2012-08-11 21:12:47Z johanjanssens $
 * @package     Nooku_Server
 * @subpackage  Application
 * @copyright   Copyright (C) 2011 - 2012 Timble CVBA and Contributors. (http://www.timble.net).
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.nooku.org
 */

$messages = $this->getView()->getMessages();
?>

<? if(count($messages)) : ?>
<dl id="system-message">
<? foreach ($messages as $type => $message) : ?>
	<dt class="<?= strtolower($type) ?>"><?= @text( $type ) ?></dt>
	<dd class="<?= strtolower($type) ?> message">
	<ul>
    <? foreach ($message as $line) : ?>
        <li><?= $line ?></li>
    <? endforeach; ?>
    </ul>
	</dd>
<? endforeach; ?>
</dl>
<? endif; ?>